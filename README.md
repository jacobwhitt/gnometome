# Gnome Tome

> A Vue.js project to facilitate trade and collaboration with the local Gnome population of Brastlewark. Gives an easy way to browse the database of all the inhabitants and their details, as well as filter one's search for specific gnomes.

> Vue-router is used to host the home page and any consequent pages that would need to be added can easily be added in the routes.js component.

> Vuex is used to control the global state which houses the initial dataset fetched from the server as well as any subsequent filtering of the initial dataset.

> Bootstrap 3 was used for all of the form and panel elements to display and filter the dataset.

> Animate.css library was used for transition animations in the main components.

## Setup and Execution

```bash
# install dependencies in the root folder from the terminal using
npm install
> Check the package.json file for more details on packages needed to run the project like vue-router, vuex,
> or babel for transpilation to anterior versions of JS pre-ecma2015

# serve with hot reload at localhost:8080
npm run dev

```
