import Vue from "vue";
import Vuex from "vuex";
import VueRouter from "vue-router";
import App from "./App.vue";
import { routes } from "./routes";

Vue.use(VueRouter);
Vue.use(Vuex);

const router = new VueRouter({
  mode: "history",
  routes,
});

const store = new Vuex.Store({
  state: {
    gnomesList: [],
    filteredList: [],
    initData: { jobs: [], hair_colors: [], minAge: 0, maxAge: 0, minHeight: 0, maxHeight: 0, minWeight: 0, maxWeight: 0 },
  },
  mutations: {
    SET_GNOMESLIST(state, census) {
      state.gnomesList = census;
      console.log("gnomesList in main.js: ", this.state.gnomesList);
    },
    SET_INITDATA(state, initData) {
      this.state.initData.jobs = initData.jobs;
      this.state.initData.hair_colors = initData.hairColors;

      this.state.initData.minAge = initData.agesArr[0];
      this.state.initData.maxAge = initData.agesArr[initData.agesArr.length - 1];

      this.state.initData.minHeight = initData.heightArr[0];
      this.state.initData.maxHeight = initData.heightArr[initData.heightArr.length - 1];

      this.state.initData.minWeight = initData.weightArr[0];
      this.state.initData.maxWeight = initData.weightArr[initData.weightArr.length - 1];
      console.log("initData in main.js: ", this.state.initData);
    },
    FILTER_GNOMESLIST(state, filteredList) {
      this.state.filteredList = filteredList;
      console.log("filteredList in main.js: ", this.state.filteredList);
    },
  },
  actions: {
    initGnomesList({ commit }) {
      fetch("https://bitbucket.org/fenix-group-international/frontend-test/raw/80d1664d5db3a516537a3bbbb4f3fca968d18b2e/data.json")
        .then((response) => response.json())
        .then((data) => {
          const census = data.Brastlewark;
          const initDataSort = {
            jobs: [],
            hairColors: [],
            agesArr: [],
            heightArr: [],
            weightArr: [],
          };

          census.forEach((gnome) => {
            //? Check for unemployment and add it to their profession if unemployed = true
            if (gnome.professions.length === 0) {
              gnome.professions = ["Unemployed"];
            }

            //? If employed, compare all jobs to store.state and add their jobs if they haven't yet been recorded.
            gnome.professions.forEach((job) => {
              if (!initDataSort.jobs.includes(job)) {
                initDataSort.jobs.push(job);
              }
            });

            //? Record their hair color, age, weight and height (only if each value is unique) to the local scope const initDataSort to be sent as payload to the mutation later
            if (!initDataSort.hairColors.includes(gnome.hair_color)) {
              initDataSort.hairColors.push(gnome.hair_color);
            }
            if (!initDataSort.agesArr.includes(gnome.age)) {
              initDataSort.agesArr.push(gnome.age);
            }
            if (!initDataSort.heightArr.includes(gnome.height)) {
              initDataSort.heightArr.push(gnome.height);
            }
            if (!initDataSort.weightArr.includes(gnome.weight)) {
              initDataSort.weightArr.push(gnome.weight);
            }
          });

          //! Sort ages, heights and weights to find min/max in data set dynamically
          const sorter = (a, b) => {
            return a - b;
          };
          initDataSort.agesArr.sort(sorter);
          initDataSort.heightArr.sort(sorter);
          initDataSort.weightArr.sort(sorter);

          commit("SET_GNOMESLIST", census);
          commit("SET_INITDATA", initDataSort);
        });
    },
    filteredList({ commit }, filters) {
      console.log("filterPayload in filteredList ACTION: ", filters);
      let census = this.state.gnomesList;
      let filtered = [];

      for (let i = 0; i < census.length; i++) {
        //* Check if each gnome meets all of the single-value filter reqs and beyond that if one of their professions matches the chosen profession(s)

        if (filters.hair_color.includes(census[i].hair_color) && census[i].age > filters.minAge && census[i].age < filters.maxAge && census[i].height > filters.minHeight && census[i].height < filters.maxHeight && census[i].weight > filters.minWeight && census[i].weight < filters.maxWeight) {
          //* if the gnome matches all the single-value filters, then we check their (multiple) professions and push them into the filtered array when at least one match is made

          if (filters.jobs.length > 0 && census[i].professions.length > 0) {
            for (let p = 0; p < census[i].professions.length; p++) {
              if (filters.jobs.includes(census[i].professions[p])) {
                filtered.push(census[i]);
                break;
              }
            }
          }
        }
      }
      //* after all the matching gnomes are pushed into the temporary 'filtered' array, it's sent as payload to the filter mutation, which will be watched by its respective computed value in the 'Gnomes.vue' component to re-render the initial 'posts' list
      commit("FILTER_GNOMESLIST", filtered);
    },
  },
  getters: {
    gnomesList: (state) => {
      return state.gnomesList;
    },
    initData: (state) => {
      return state.initData;
    },
    filteredList: (state) => {
      return state.filteredList;
    },
  },
});

new Vue({
  el: "#app",
  router,
  store,
  render: (h) => h(App),
});
